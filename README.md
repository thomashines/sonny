Sonny is a 2D G-code generator intended to make sketches of image files using pens.
The first goals are primitive shapes: lines, rectangles, regular polyhedron, curves; and sketching bitmap images in a style similar to Sonny's in that one scene from I, Robot.

To use:
$ make sonny && sonny test/RepRap-inverted.png > test/RepRap-inverted.gcode