package gcode

import (
	"fmt"
)

type GCode struct {
	HomeX, HomeY float64
	RaiseHeight float64
	RaiseRate float64
	MoveRate float64
	DrawRate float64
	XYRate float64

	X, Y, Z float64
	Raised bool
	RaiseWithServo bool
	ServoID int

	Actions []Action
}

type Action struct {
	Description string
	Code string
}

func (gc *GCode) EchoGCode() {
	/*
	G0  : Move
		G1 F1500 ; Set feed rate to 1500
		G1 X50 Y25.3 ; Move to X=50 and Y=25.3 at feed rate
	G21 : Units (mm)
	G28 : Home
		G28 X Y ; Move to home on X and Y axis
	G90 : Coords (absolute)
	M84 : Disable motors
	M280: Servo
		M280 P0 S135 ; Set servo 0 to 135 degrees
	*/
	
	// Header
	fmt.Printf("G21 ; Set units to millimeters\n")
	fmt.Printf("G90 ; Set to absolute coordinates\n")
	fmt.Printf("G0 F60 ; Set feed rate\n")
	
	fmt.Printf("\n")

	for line, action := range gc.Actions {
		fmt.Printf(
			"%-35s ; Line %4d %-10s\n",
			action.Code,
			line,
			action.Description)
	}

	fmt.Printf("\n")
	
	// Footer
	fmt.Printf("M84 ; disable motors\n")
}

func (gc *GCode) Do(action Action) {
	gc.Actions = append(
		gc.Actions,
		action)
}

func (gc *GCode) MoveTo(description string, x, y, z, rate float64) {
	gc.Do(Action{
		Code: fmt.Sprintf(
			"G0 X%06.2f Y%06.2f Z%06.2f F%07.2f",
			x,
			y,
			z,
			60.0*rate),
		Description: description})
	gc.X = x
	gc.Y = y
	gc.Z = z
}

func (gc *GCode) MoveAbsXY(x, y float64) {
	gc.MoveTo(
		"MoveAbsXY",
		x,
		y,
		gc.Z,
		gc.XYRate)
}

func (gc *GCode) MoveRelXY(dx, dy float64) {
	gc.MoveTo(
		"MoveRelXY",
		gc.X + dx,
		gc.Y + dy,
		gc.Z,
		gc.XYRate)
}

func (gc *GCode) HomeXY() {
	gc.Do(Action{
		Description: "HomeXY",
		Code: "G28 X Y"})
	gc.X = gc.HomeX
	gc.Y = gc.HomeY
}

func (gc *GCode) HomeZ() {
	gc.Do(Action{
		Description: "HomeZ",
		Code: "G28 Z"})
	gc.Z = 0.0
}

func (gc *GCode) RaiseZ() {
	gc.MoveTo(
		"RaiseZ",
		gc.X,
		gc.Y,
		gc.RaiseHeight,
		gc.RaiseRate)
}

func (gc *GCode) LowerZ() {
	gc.MoveTo(
		"LowerZ",
		gc.X,
		gc.Y,
		0.0,
		gc.RaiseRate)
}

func (gc *GCode) RaiseServo() {
	gc.WaitOnActions()
	gc.Do(Action{
		Description: "RaiseServo",
		Code: fmt.Sprintf(
			"M280 P%d S045",
			gc.ServoID)})
	gc.Dwell(200.0)
}

func (gc *GCode) LowerServo() {
	gc.WaitOnActions()
	gc.Do(Action{
		Description: "LowerServo",
		Code: fmt.Sprintf(
			"M280 P%d S135",
			gc.ServoID)})
	gc.Dwell(200.0)
}

func (gc *GCode) Raise() {
	if !gc.Raised {
		if gc.RaiseWithServo {
			gc.RaiseServo()
		} else {
			gc.RaiseZ()
		}
		gc.Raised = true
		gc.XYRate = gc.MoveRate
	}
}

func (gc *GCode) Lower() {
	if gc.Raised {
		if gc.RaiseWithServo {
			gc.LowerServo()
		} else {
			gc.LowerZ()
		}
		gc.Raised = false
		gc.XYRate = gc.DrawRate
	}
}

func (gc *GCode) Dwell(milliseconds float64) {
	gc.Do(Action{
		Description: "Dwell",
		Code: fmt.Sprintf(
			"G4 P%06.2f",
			milliseconds)})
}

func (gc *GCode) WaitOnActions() {
	gc.Do(Action{
		Description: "WaitOnActions",
		Code: "M400"})
}


func (gc *GCode) Start() {
	gc.HomeZ()
	gc.Raise()
	gc.HomeXY()
}

func (gc *GCode) Finish() {
	gc.Raise()
	gc.HomeXY()
	if gc.RaiseWithServo {
		gc.LowerServo()
		gc.RaiseZ()
	}
}