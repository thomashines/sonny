package main

import (
	"fmt"
	"image"
	_ "image/png"
	"os"

	"bitbucket.org/spark4160/sonny/gcode"
)

func main() {
	fmt.Printf("; Sonny G-code generator.\n")
	fmt.Printf("; START\n")
	// Set variables
	gc := gcode.GCode{
		HomeX: 0,
		HomeY: 270,
		RaiseHeight: 2,
		MoveRate: 100,
		DrawRate: 100,
		RaiseRate: 3,
		RaiseWithServo: true,
		ServoID: 0,
	}
	// Home
	gc.Start()

	// Draw rectangle
	// gc.MoveRel(20, -20) // To position
	// gc.Lower()
	// gc.MoveRel(10, 0) // Right
	// gc.MoveRel(0, -10) // Down
	// gc.MoveRel(-10, 0) // Left
	// gc.MoveRel(0, 10) // Up

	// Load image
	image_file, err := os.Open(os.Args[1])
	if err != nil {panic(err)}
	defer image_file.Close()

	image_src, _, err := image.Decode(image_file)
	if err != nil {	panic(err) }

	// Image size, pixels/mm
	dpmm := 20.0

	// Stroke width, mm
	line_width := 0.5

	// Top left
	min_x := 30.0
	max_y := 270.0

	RenderBitmap(image_src,
		&gc,
		dpmm,
		line_width,
		min_x,
		max_y)

	// Home
	gc.Finish()
	// gc.Print()
	gc.EchoGCode()
	fmt.Printf("\n; END\n")
}