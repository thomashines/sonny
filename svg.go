package main

import (
	"fmt"
	"image"
	"image/color"
	_ "image/png"
	"math"

	"bitbucket.org/spark4160/sonny/gcode"
)

func ColorToString(color color.Color) string {
	r, g, b, _ := color.RGBA()
	color_string := fmt.Sprintf("%05d %05d %05d", r, g, b)
	switch color_string {
		case "00000 00000 00000":
			return "Black"
		case "65535 65535 65535":
			return "White"
		case "13878 13878 13878":
			return "Gray"
		default:
			return color_string
	}
}

func RenderBitmap(image_src image.Image,
                  gc *gcode.GCode,
                  dpmm float64,
                  line_width float64,
                  min_x float64,
                  max_y float64) {
	bounds := image_src.Bounds()
	width, height := bounds.Max.X, bounds.Max.Y
	fmt.Printf("; Origin (top left) %s\n", ColorToString(image_src.At(0, 0)))
	
	drawn := make([][]bool, width)
	for x := 0; x < width; x++ {
		drawn[x] = make([]bool, height)
	}

	// Move to top left corner
	gc.MoveAbsXY(min_x - 1.0, max_y + 1.0)
	
	// x_step := int(math.Ceil(line_width*dpmm)) // Skip the width of the stroke
	x_step := 1
	y_step := int(math.Ceil(line_width*dpmm)) // Skip the width of the stroke

	draw_from_x, draw_to_x := width - 1, 0 // X pixel to start drawing from

	drew := true // Flag set when branched
	draw_count := 0

	for drew {
		drew = false
		// Search for undrawn black
		// Draw until congruity breaks
		// Repeat until nothing left to draw
		for y := 0; y < height; y += y_step {
			y_mm := max_y - float64(y)/dpmm

			// Start at the end x of the previous line
			// If black and undrawn:
			//  Search in the previous direction for the start of the
			//  undrawn black
			// If white or drawn:
			//  Search in the current direction for the start of the
			//  undrawn black
			// Draw to this point
			// Search in the current direction for the end of the black
			// Draw to this point
			
			// Start at the end x of the previous line
			color := ColorToString(image_src.At(draw_to_x, y))
			will_draw := false

			x_search_step := 0

			// If black and undrawn:
			if color == "Black" && !drawn[draw_to_x][y] {
				//  Search in the previous direction for the start of the
				//  undrawn black
				x_search_step = +x_step
				will_draw = true
			}
			
			// If white or drawn:
			if color != "Black" || drawn[draw_to_x][y] {
				//  Search in the current direction for the start of the
				//  undrawn black
				x_search_step = -x_step
			}

			for x := draw_to_x;
			    x >= 0 && x < width;
			    x += x_search_step {

				// Search for start of the undrawn black
				search_color := ColorToString(image_src.At(x, y))
				
				// Currently in undrawn black, searching for drawn pixels
				// or white
				if (color == "Black" && !drawn[draw_to_x][y]) &&
				   (search_color != "Black" || drawn[x][y]) {
					will_draw = true
					draw_from_x = x - x_search_step
					break
				}

				// Currently in drawn pixels or white, searching for
				// undrawn black
				if color != "Black" || drawn[draw_to_x][y] {
					// Moved beyond start of previous line
					if (x_search_step > 0 && x > draw_from_x + x_search_step) ||
					   (x_search_step < 0 && x < draw_from_x + x_search_step) {
					   	break
					}

					// Found undrawn black
				  if search_color == "Black" && !drawn[x][y] {
				  	will_draw = true
						draw_from_x = x
						break
					}
				}

			}

			// Reverse directions
			x_step = -x_step

			if will_draw {
				
				// Have found undrawn black to draw
				gc.MoveAbsXY(float64(draw_from_x)/dpmm + min_x, y_mm)
				if !drew {
					// Haven't started drawing this loop
					// Start drawing
					gc.Lower()
				}

				// Search in new direction for end of undrawn black
				for draw_to_x = draw_from_x;
				    draw_to_x >= 0 && draw_to_x < width;
				    draw_to_x += x_step {
					color := ColorToString(image_src.At(draw_to_x, y))
					if color != "Black" || drawn[draw_to_x][y] {
						break
					}
				}
				draw_to_x -= x_step
				gc.MoveAbsXY(float64(draw_to_x)/dpmm + min_x, y_mm)

				drawn_step := 1
				if x_step < 0 {
					drawn_step = -1
				}
				for x := draw_from_x;
				    x >= 0 && x < width;
				    x += drawn_step {
				  if x == draw_to_x + drawn_step {
				  	break
				  }
					drawn[x][y] = true
				}

				drew = true
			} else {
				if drew {
					// Have started drawing, but have now stopped
					// Raise the pen
					gc.Raise()
					// Break this loop, restart from top
					break
				}
				if x_step > 0 {
					// Would have drawn toward the right
					draw_from_x = 0
					draw_to_x = width - 1
				} else {
					// Would have drawn toward the left
					draw_from_x = width - 1
					draw_to_x = 0
				}
			}
		}
		draw_count++
	}

	fmt.Printf("; Drawing iterations %d\n", draw_count)
	fmt.Printf("\n")
}